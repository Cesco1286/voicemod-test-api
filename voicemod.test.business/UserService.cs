﻿using System;
using voicemod.test.business.Exceptions;
using voicemode.test.repository;
using voicemode.test.repository.Entity;

namespace voicemod.test.business
{
    public class UserService
    {
        private UserRepository _repository;

        public UserService(UserRepository repository)
        {
            _repository = repository;
        }

        public bool Login(string username, string password)
        {
            var user = _repository.GetUserByUsername(username);
            if(user == null)
            {
                return false;
            }
            return user.Password.Equals(password);            
        }

        public User GetUserById(string id)
        {
            var user = _repository.GetUserById(id);
            if (user == null)
            {
                throw new NotExistingUserException("The provided user does not exist");
            }
            return user;
        }

        public string Create(string username, string password,  string name, string firstSurname, string secondSurname, string country, string telephone, string postalCode)
        {
            var user = _repository.GetUserByUsername(username);
            if(user != null)
            {
                throw new Exception("this username already exists");
            }
            var id = Guid.NewGuid().ToString();
            var newUser = new User
            {
                Id = id,
                Name = name,
                Email = username,
                Password = password,
                FirstSurname = firstSurname,
                SecondSurname = secondSurname,
                Country = country,
                TelephoneNumber = telephone,
                PostalCode = postalCode
            };

            return _repository.AddUser(newUser);

        }


        public bool Update(string id,string username, string password, string name, string firstSurname, string secondSurname, string country, string telephone, string postalCode)
        {
            var user = _repository.GetUserById(id);
            if (user == null)
            {
                throw new NotExistingUserException("The provided user does not exist");
            }            
            var newUser = new User
            {
                Id = id,
                Name = name,
                Email = username,
                Password = password,
                FirstSurname = firstSurname,
                SecondSurname = secondSurname,
                Country = country,
                TelephoneNumber = telephone,
                PostalCode = postalCode
            };

            return _repository.UpdateUser(newUser);
        }
        public bool Delete(string id)
        {                
            return _repository.DeleteUser(id);
        }

    }
}
