﻿using System;
using System.Collections.Generic;
using System.Text;

namespace voicemod.test.business.Exceptions
{
    public class NotExistingUserException : Exception
    {
        public NotExistingUserException(string message) : base(message) { }
    }
}
