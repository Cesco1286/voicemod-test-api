﻿using System;

namespace voicemode.test.commons
{
    public class User
    {
        public Guid Id { get; set; }

        public String Name { get; set; }

        public String Description { get; set; }

        public bool LimitUser { get; set; }

        public String Domain { get; set; }

        public String Theme { get; set; }

        public class Tenant
    {
        public Guid Id { get; set; }

        public String Name { get; set; }

        public String Description { get; set; }

        public bool LimitUser { get; set; }

        public String Domain { get; set; }

        public String Theme { get; set; }

    }

    }
}
