﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using voicemode.test.repository.Entity;
using MySql.Data.MySqlClient;

namespace voicemode.test.repository
{
    public class UserRepository
    {
        string _dbConnString;

        MySqlConnection _mysqlConn;

        public UserRepository(string connectionString)
        {

            _dbConnString = connectionString;
            _mysqlConn = new MySqlConnection(connectionString);
            var createTable = @"CREATE TABLE IF NOT EXISTS `user` (
  `id` varchar(36) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `Name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `FirstSurname` varchar(100) COLLATE utf8_bin NOT NULL,
  `SecondSurname` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `Email` varchar(100) COLLATE utf8_bin NOT NULL,
  `Password` varchar(100) COLLATE utf8_bin NOT NULL,
  `Country` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `TelephoneNumber` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `PostalCode` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `CreatedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_bin;";
            _mysqlConn.Execute(createTable);

        }


        public User GetUserByUsername(string username)
        {
            return _mysqlConn.QueryFirstOrDefault<User>("Select * from user where Email = @Email", new { Email = username });
        }

        public User GetUserById(string id)
        {
            return _mysqlConn.QueryFirstOrDefault<User>("SELECT * FRom user WHERE Id = @Id", new { Id = id});
        }




        public string AddUser(User user)
        {

            _mysqlConn.Execute("INSERT INTO user (Id,Name,FirstSurname,SecondSurname,Email, Password,Country,TelephoneNumber,PostalCode) VALUES (@Id,@Name,@FirstSurname,@SecondSurname,@Email, @Password,@Country,@TelephoneNumber,@PostalCode)", new
            {
                Id = user.Id,
                user.Name,
                user.FirstSurname,
                user.SecondSurname,
                user.Email,
                user.Password,
                user.Country,
                user.TelephoneNumber,
                user.PostalCode
            });
            return user.Id;
        }

        public bool UpdateUser(User user)
        {
            var res = _mysqlConn.Execute("UPDATE user SET Name=@Name,FirstSurname=@FirstSurname, SecondSurname=@SecondSurname, Email=@Email, Password=@Password, Country=@Country, TelephoneNumber=@TelephoneNumber, PostalCode=@PostalCode WHERE Id=@Id",                
                new
                {
                    Id = user.Id,
                    Name = user.Name,
                    FirstSurname = user.FirstSurname,
                    SecondSurname = user.SecondSurname,
                    Email = user.Email,
                    Password = user.Password,
                    Country = user.Country,
                    TelephoneNumber = user.TelephoneNumber,
                    PostalCode = user.PostalCode
                });
            return res == 1;
        }

        public bool DeleteUser(string id)
        {
            return _mysqlConn.Execute("DELETE FROM user WHERE Id=@Id",
                new
                {
                    Id = id
                }) == 1;
        }

    }
}
