﻿using System;
using System.Collections.Generic;
using System.Text;

namespace voicemode.test.repository.Entity
{
    public class User
    {
        
        public string Id { get; set; }
        public string Name { get; set; }
        public string FirstSurname { get; set; }
        public string SecondSurname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Country { get; set; }
        public string TelephoneNumber { get; set; }
        public string PostalCode { get; set; }               
        public DateTime CreatedAt { get; set; }
    }
}
