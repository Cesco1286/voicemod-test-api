
To be able to run the project, clone the repository and install Microsoft .net Core SDK from this link: https://dotnet.microsoft.com/download. 
To actually run it, open the file "voicemod.be.test.sln" in Visual Studio and press F5. External dependencies will be resolved automatically.

The API is connected to an MySQL database cloud instance; once the free trial period is over, connections to the database will be refused.

The swagger documentation is the homepage. You can try the methods that way.


