﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using voicemode.test.api.Requests;
using voicemod.test.business;

namespace voicemode.test.api.Controllers
{
    [Produces("application/json")]
    [Route("api/User")]
    public class UserController : Controller
    {
        private UserService _userService;

        public UserController(UserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody]LoginRequest loginData)
        {

            if (String.IsNullOrEmpty(loginData.Email) || String.IsNullOrEmpty(loginData.Password))
            {
                return new BadRequestObjectResult("email or password missing");
            }
            var loginResult = _userService.Login(loginData.Email, loginData.Password);
            if (loginResult)
            {
                return new OkObjectResult("Login Ok");
            }
            else
            {
                return new BadRequestObjectResult("Invalid password/username");
            }
        }

        [HttpPost]
        [Route("create")]
        public async Task<IActionResult> Create([FromBody]UserCreateRequest loginData)
        {
            try
            {
                var result = _userService.Create(loginData.Email, loginData.Password, loginData.Name, loginData.FirstSurname, loginData.SecondSurname, loginData.Country, loginData.TelephoneNumber, loginData.PostalCode);
                return new OkObjectResult(new { result = "Success", data = new { id = result }  });
            }
            catch (Exception e)
            {
                return new BadRequestObjectResult(e.Message);
            }
        }

        [HttpPost]
        [Route("delete")]
        public async Task<IActionResult> Delete([FromBody]UserDeleteRequest loginData)
        {

            try
            {
                var result = _userService.Delete(loginData.Id);
                if (result)
                {
                    return new OkObjectResult("User deleted successfully");
                }
                else
                {
                    return new BadRequestObjectResult("impossible to delete user");
                }

            }
            catch (Exception e)
            {
                return new BadRequestResult();
            }

        }

        [HttpPost]
        [Route("update")]
        public async Task<IActionResult> Update([FromBody]UserUpdateRequest loginData)
        {
            try
            {
                var result = _userService.Update(loginData.Id, loginData.Email, loginData.Password, loginData.Name, loginData.FirstSurname, loginData.SecondSurname, loginData.Country, loginData.TelephoneNumber, loginData.PostalCode);
                if (result)
                {
                    return new OkObjectResult("User updated");
                }
                else
                {
                    return new BadRequestObjectResult("impossible to update user");
                }

            }
            catch (Exception e)
            {
                return new BadRequestResult();
            }
        }


    }




}