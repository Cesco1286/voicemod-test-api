﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using voicemode.test.repository;
using voicemod.test.business;

namespace voicemode.test.api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {            
            services.AddTransient<UserRepository, UserRepository>(s => new UserRepository(Configuration.GetConnectionString("dbConnection")));
            services.AddTransient<UserService, UserService>();
            services.AddMvc();
            services.AddSwaggerGen(c =>
           {
               c.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info
               {
                   Version = "v1",
                   Title = "Voicemod - User Api Documentation",
                   Description = "",
                   TermsOfService = "None",
                   Contact = new Swashbuckle.AspNetCore.Swagger.Contact()
                   {
                       Name = "Francesco Delussu",
                       Email = "delussu.fra@gmail.com",
                       Url = "https://www.linkedin.com/in/francesco-delussu-1a0000124/"
                   }
               });
           });


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Voicemod User Api");
            });

        }
    }
}
